#!/bin/bash

# Variables
usuario=$(whoami)
puertoWeb=7000
#-----------



function main {
    echo "Hola, $usuario, vamos a preparar el entorno"

    

    #Iniciar websocket
    gnome-terminal --tab --title="Websocket" --command="roslaunch rosbridge_server rosbridge_websocket.launch"

    # Iniciar Servidor Web
    gnome-terminal --tab --title="Web Python" --command="python -m SimpleHTTPServer $puertoWeb"

    #Iniciar exploracion
    gnome-terminal --tab --title="Exploracion" --command="roslaunch stack_de_navegacion stack_de_navegacion.launch"

    #Iniciar procesos ROS
    gnome-terminal --tab --title="MaquinaEstados" --command="roslaunch maquina_estados maquina_estados.launch" && gnome-terminal --tab --title="AWS" --command="roslaunch aws_lex aws_lex.launch"

    #Iniciar bow aws-lex
    gnome-terminal --tab title="Aws-lex" --command="roslaunch aws_lex aws_lex.launch"

    #Iniciar relocator
    gnome-terminal --tab --title="Relocator" --command="roslaunch relocation_service relocation_service.launch"
    
    #Iniciar relocator
    gnome-terminal --tab --title="OpenCv" --command="roslaunch seguir_pelota seguir_pelota.launch"

}



main
