server_ip = location.hostname;

window.onload = function () {

    let vueApp = new Vue({
        el: "#vueApp",
        data: {
            // ros connection
            ros: null,
            rosbridge_address: 'ws://' + server_ip + ':9090/',
            connected: false,
            // page content
            procesando: false,
            happy: "happy_robot_sound",
            menu_title: 'Connection',
            main_title: 'Main title, from Vue!!',
        },

        methods: {
            connect() {
                // define ROSBridge connection object
                this.procesando = false

                this.ros = new ROSLIB.Ros({
                    url: this.rosbridge_address
                })

                // define callbacks
                this.ros.on('connection', () => {
                    this.connected = true
                    console.log('Connection to ROSBridge established!')

			//this.setCamera()
                })

                this.ros.on('error', (error) => {
                    console.log('Something went wrong when trying to connect')
                    this.disconnect()
                    console.log(error)
                })

                this.ros.on('close', () => {
                    this.connected = false
                    console.log('Connection to ROSBridge was closed!')

			//document.getElementById('divCamera').innerHTML=''
                })
            },



	/*
		setCamera: function(){


		console.log('Setting the camera')
		let viewer1=new JPEGCANVAS.Viewer({

		divID: 'divCamera',
		host: 'ws://' + server_ip + ':9090/',
		width:320,
		height:240,
		topic:'/turtlebot3/camera/image_raw',
		ssl:false
		})

		},

	*/


            llamar_maquina_estados: function(message, a, b, c){

                this.procesando = true
                this.service_response=''

                // Definir el SERVICIO a llamar
                let service = new ROSLIB.Service({
                    ros: this.ros,
                    name: '/maquina_estados',
                    serviceType: 'maquina_estados/service_message'
                })

                // Definir la peticion; value a enviar
                let request = new ROSLIB.ServiceRequest({
                    message: message,
                    A: a,
                    B: b,
                    C: c,
                })

                // Definir el callback
                service.callService(request, (result) => {
                    this.procesando = false
                    console.log(this.procesando)
                    this.service_response = JSON.stringify(result)
                },  (error) => {
                    this.procesando = false
                    console.error(error)
                })

            },

            disconnect: function() {
                this.ros.close()
            },
            sendMoveFoward: function (){
                let topic = new ROSLIB.Topic({
                    ros: this.ros,
                    name: '/cmd_vel',
                    messageType: 'geometry_msgs/Twist'
                })
                let message = new ROSLIB.Message({
                    linear: {x: 0.2, y: 0, z:0,},
                    angular: {x:0, y:0, z:0,},
                })
                topic.publish(message)
            },
            sendMoveRight: function (){
                let topic = new ROSLIB.Topic({
                    ros: this.ros,
                    name: '/cmd_vel',
                    messageType: 'geometry_msgs/Twist'
                })
                let message = new ROSLIB.Message({
                    linear: {x: 0, y: 0.0, z:0,},
                    angular: {x:0, y:0, z:-0.5,},
                })
                topic.publish(message)
            },
            sendMoveLeft: function (){
                let topic = new ROSLIB.Topic({
                    ros: this.ros,
                    name: '/cmd_vel',
                    messageType: 'geometry_msgs/Twist'
                })
                let message = new ROSLIB.Message({
                    linear: {x: 0, y: 0, z:0,},
                    angular: {x:0, y:0, z:0.5,},
                })
                topic.publish(message)
            },
            sendMoveBack: function (){
                let topic = new ROSLIB.Topic({
                    ros: this.ros,
                    name: '/cmd_vel',
                    messageType: 'geometry_msgs/Twist'
                })
                let message = new ROSLIB.Message({
                    linear: {x: -0.2, y: 0, z:0,},
                    angular: {x:0, y:0, z:0,},
                })
                topic.publish(message)
            },
            sendStop: function (){
                let topic = new ROSLIB.Topic({
                    ros: this.ros,
                    name: '/cmd_vel',
                    messageType: 'geometry_msgs/Twist'
                })
                let message = new ROSLIB.Message({
                    linear: {x: 0, y: 0, z:0,},
                    angular: {x:0, y:0, z:0,},
                })
                topic.publish(message)
            },

	jugar(){


	if(document.getElementById('botonExplorar').innerHTML=="Jugar"){




	console.log('Empeando a jugar')
		this.service=null
            this.service_busy = true
            this.service_response = false
            // define the Service to call
            this.service = new ROSLIB.Service({
                ros: this.ros,
                name: '/jugar',
                serviceType: 'pelota_msg/MyCustomServiceMessage'
             })
            // define the request
		this.request=null




           this.request = new ROSLIB.ServiceRequest({
                arrancar: 1
            })
            // define a callback
            this.service.callService(this.request, (result) => {
                this.service_busy = false
                this.service_response = JSON.stringify(result)
	        console.log(this.service_response)
            }, (error) => {
                this.service_busy = false
                console.error(error)
            })


		document.getElementById('botonExplorar').innerHTML="Parar"

		}//if

		else{



		console.log('Parando de jugar')
		this.service=null
            this.service_busy = true
            this.service_response = false
            // define the Service to call
            this.service = new ROSLIB.Service({
                ros: this.ros,
                name: '/jugar',
                serviceType: 'pelota_msg/MyCustomServiceMessage'
             })
            // define the request
		this.request=null




           this.request = new ROSLIB.ServiceRequest({
                arrancar: 0
            })
            // define a callback
            this.service.callService(this.request, (result) => {
                this.service_busy = false
                this.service_response = JSON.stringify(result)
	        console.log(this.service_response)
            }, (error) => {
                this.service_busy = false
                console.error(error)
            })


		document.getElementById('botonExplorar').innerHTML="Jugar"

           }
    }


        },


        mounted() {
            console.log('pagina lista!')
        },
    })

    vueApp.connect()
}



//var botonJugar = document.getElementById('botonExplorar');
//botonJugar.onclick=function(){mapa.jugar()};
