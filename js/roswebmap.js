
class Mapa{
    constructor(address, port, mapDiv, width, height){
        this.rosbridge_address = address;
        this.port = port;
        this.map = mapDiv;
        this.connected = false;
        this.ros = null;
        this.loading = false;
        this.mapViewer = null;
        this.mapGridClient =  null;
        this.interval =  null;
        this.logs = []
        this.width = width;
        this.height = height;
	
        this.ros = new ROSLIB.Ros({
            url: this.rosbridge_address
        })

        try {
            this.connect()
        } catch(error){
            console.error('Error: ', error)
        }
    }

   

    connect(){
        this.loading = true
            
           this.ros.on('connection', () => {
                this.logs.unshift((new Date()).toTimeString() + ' - Connected!')
                this.connected = true
                this.loading = false

                this.mapViewer = new ROS2D.Viewer({
                    divID: this.map,
                    width: this.width,
                    height: this.height,
                    zoom: 14
                })

                // Setup the map client.
                this.mapGridClient = new ROS2D.OccupancyGridClient({
                    ros: this.ros,
                    rootObject: this.mapViewer.scene,
                    continuous: true,
                })
                // Scale the canvas to fit to the map
                this.mapGridClient.on('change', () => {
                    this.mapViewer.scaleToDimensions(this.mapGridClient.currentGrid.width, this.mapGridClient.currentGrid.height);
                    this.mapViewer.shift(this.mapGridClient.currentGrid.pose.position.x, this.mapGridClient.currentGrid.pose.position.y)
                })

                this.mapGridClient.on('click', (e) => {
                    console.log("Un click en el gridclient")
                })

            })
            this.ros.on('error', (error) => {
                this.logs.unshift((new Date()).toTimeString() + ` - Error: ${error}`)
            })
            this.ros.on('close', () => {
                this.logs.unshift((new Date()).toTimeString() + ' - Disconnected!')
                this.connected = false
                this.loading = false
                document.getElementById('mapa').innerHTML = ''
            })
    }
     
     explorar(segundos){

	console.log('Hola')
		this.service=null
            this.service_busy = true
            this.service_response = false
            // define the Service to call
            this.service = new ROSLIB.Service({
                ros: this.ros,
                name: '/explorar',
                serviceType: 'exploration_msg/MyCustomServiceMessage'
             })
            // define the request
		this.request = null
           this.request = new ROSLIB.ServiceRequest({
                segundos: segundos
            })
            // define a callback
            this.service.callService(this.request, (result) => {
                this.service_busy = false
                this.service_response = JSON.stringify(result)
	        console.log(this.service_response)
            }, (error) => {
                this.service_busy = false
                console.error(error)
            })
		

        }


	

	




        relocalizar(x,y){
            this.servicio = null;
            this.servicio_busy = true
            this.servicio_respuesta = false

            this.servicio = new ROSLIB.Service({
                ros:this.ros,
                name: '/relocation_service',
                serviceType: 'relocation_service/location_request'
            })
            this.peticion = null;
            this.peticion = new ROSLIB.ServiceRequest({
                coordX:x,
                coordY:y
            })

            this.servicio.callService(this.peticion, (result) =>{
                this.servicio_busy = false
                this.servicio_respuesta = JSON.stringify(result)
                console.log(this.servicio_respuesta)
            }, (error)=>{
                this.servicio_busy = false
                console.error(error)
            })
        }

    disconnect(){
        this.ros.close();
    }

    getLogs(){
        return this.logs;
    }
}


server_ip = location.hostname;


var mapaWidth = document.getElementById('mapa').getBoundingClientRect().width;
var mapaHeight = Math.round(mapaWidth*(screen.height/screen.width)*0.80);
var botonExplorar = document.getElementById('botonHacerMapaContainer');

var mapa = new Mapa('ws://' + server_ip + ':9090', '9090','mapa', mapaWidth, mapaHeight)
console.log(mapaWidth + " : " + mapaHeight)

var segundos = document.getElementById("input").value; 
botonExplorar.onclick = function(){mapa.explorar(parseInt(segundos))};


let mapaDoc = document.getElementById("mapa");

let coords = document.getElementById("coords")

let isTapping = false;

let currentX = 0;
let currentY = 0;

// Delimitación del mapa en el canvas
let maxMapaX = 222; // 540
let minMapaX = 128; // 300
let maxMapaY = 248; // 630
let minMapaY = 133; // 330



//Servicio
// Evento toque del mapa
mapaDoc.addEventListener('touchstart', updateCoordenadas)
mapaDoc.addEventListener('mousedown', updateCoordenadasMouse)
mapaDoc.addEventListener('touchmove', updateCoordenadas)
mapaDoc.addEventListener('touchend', sendCoordsToRos)
mapaDoc.addEventListener('mouseup', sendCoordsToRos)

function updateCoordenadas(event) {
    let coordenadas = getCoords(event);
    coords.innerHTML = "X: " + coordenadas.x + "  Y  :" + coordenadas.y;
    currentX = coordenadas.x;
    currentY = coordenadas.y;
}

function updateCoordenadasMouse(event){
    let coordenadas = getCoordsMouse(event);
    coords.innerHTML = "X: " + coordenadas.x + "  Y  :" + coordenadas.y;
    currentX = coordenadas.x;
    currentY = coordenadas.y;
}

function sendCoordsToRos(){
    let relativeX = getRelativeCoord(currentX ,true);
    let relativeY = getRelativeCoord(currentY, false);
    console.log("X: " + relativeX + "  Y:" + relativeY)
    mapa.relocalizar(relativeX, relativeY)
}

function getCoords(event){
    let rect = mapaDoc.getBoundingClientRect();
    let xRelativo = Math.round(event.touches[0].clientX - rect.x);
    let yRelativo = Math.round(event.touches[0].clientY - rect.y);

    if (xRelativo > mapaWidth) xRelativo = mapaWidth;
    if (xRelativo < 0) xRelativo = 0;

    if (yRelativo > mapaHeight) yRelativo = mapaHeight;
    if (yRelativo < 0) yRelativo = 0;

    return {y:yRelativo, x:xRelativo}
}

function getCoordsMouse(event){
    let rect = mapaDoc.getBoundingClientRect();
    let xRelativo = Math.round(event.clientX - rect.x);
    let yRelativo = Math.round(event.clientY - rect.y);

    if (xRelativo > mapaWidth) xRelativo = mapaWidth;
    if (xRelativo < 0) xRelativo = 0;

    if (yRelativo > mapaHeight) yRelativo = mapaHeight;
    if (yRelativo < 0) yRelativo = 0;

    return {y:yRelativo, x:xRelativo}
}

function getRelativeCoord(coord, isWidth) {
    if (isWidth) {
        if(coord > maxMapaX) coord = maxMapaX;
        if(coord < minMapaX) coord = minMapaX;
        return (coord - minMapaX)/(maxMapaX - minMapaX);
    }

    if (coord > maxMapaY) coord = maxMapaY;
    if (coord < minMapaY) coord = minMapaY;

    return (coord - minMapaY)/(maxMapaY - minMapaY);
}
