server_ip = location.hostname;

window.onload = function () {

    let vueApp = new Vue({
        el: "#vueApp",
        data: {
            // ros connection
            ros: null,
            rosbridge_address: 'ws://' + server_ip + ':9090/',
            connected: false,
            // topic data
            response: "",
            input: "",
            // page content
            menu_title: 'This a connection',
            main_title: 'And this a Tittle, yoh, from Vue!!',
        },

        methods:{
             connect: function() {
                // define ROSBridge connection object
                this.ros = new ROSLIB.Ros({
                    url: this.rosbridge_address
                })

                // define callbacks
                this.ros.on('connection', () => {
                    this.connected = true
                    console.log('Connection to ROSBridge established!')
                    let topic = new ROSLIB.Topic({
                        ros: this.ros,
                        name: '/text_output',
                        messageType: 'std_msgs/String'
                    })

                    topic.subscribe((message)=>{
                        this.response = message.data
                        console.log(message)
                    })
                })

                this.ros.on('error', (error) => {
                    console.log('Something went wrong when trying to connect')
                    console.log(error)
                })
                this.ros.on('close', () => {
                    this.connected = false
                    console.log('Connection to ROSBridge was closed!')
                })
            },

            sendInput: function() {

                console.log(this.input)
                var textInput = new ROSLIB.Topic({
                    ros: this.ros,
                    name: '/text_input',
                    messageType: 'std_msgs/String'
                })
                let message = new ROSLIB.Message({
                    data: this.input  // SI NO VA puede que sea por esto
                })
                textInput.publish(message)
            },


            disconnect: function() {
                this.ros.close()
            },
        },

        mounted() {
            console.log('pagina lista!')
        },
    })

    vueApp.connect()
}